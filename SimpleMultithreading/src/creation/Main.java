package creation;


import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

    public static void main(String[] args) {

        final int COUNT = 10;
        final Thread thread1 = new SimpleThread(COUNT, "Thread1 -> SimpleThread -> ");
        final Thread thread2 = new Thread(new SimpleRunnable(COUNT, "Thread2 -> SimpleRunnable -> "));
        final ExecutorService executorService = Executors.newCachedThreadPool();
        final ExecutorService executorServiceCallable = Executors.newSingleThreadExecutor();

        thread1.start();
        thread2.start();

        for(int i = 0; i < 3; i++)
            executorService.execute(new SimpleRunnable(COUNT, "Executor" + i + " -> "));
        executorService.shutdown();

        Future<String> fs = executorServiceCallable.submit(new SimpleCallable(COUNT, "Callable -> "));
        try {
            fs.get();  //blocks until completion
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        finally {
            executorServiceCallable.shutdown();
        }

        printMessage(COUNT);
    }

    private static void printMessage(int count) {
        for (int i = 0; i < count; i++) {
            try {
                System.out.println("Hello from Main thread -> " + i);
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
