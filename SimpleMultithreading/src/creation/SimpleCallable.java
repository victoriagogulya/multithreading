package creation;


import java.util.concurrent.Callable;

public class SimpleCallable implements Callable<String> {

    private final int count;
    private final String message;

    public SimpleCallable(int count, String message) {
        this.count = count;
        this.message = message;
    }

    @Override
    public String call() throws Exception {
        for (int i = 0; i < count; i++) {
            try {
                System.out.println(message + i);
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return message + "Done!";
    }
}
