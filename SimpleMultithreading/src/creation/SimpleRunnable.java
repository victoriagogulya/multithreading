package creation;


public class SimpleRunnable implements Runnable {

    private final int count;
    private final String message;

    public SimpleRunnable(int count, String message) {
        this.count = count;
        this.message = message;
    }
    @Override
    public void run() {
        for (int i = 0; i < count; i++) {
            try {
                System.out.println(message + i);
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
