package creation;


public class SimpleThread extends Thread {

    private final int count;
    private final String message;

    public SimpleThread(int count, String message) {
        this.count = count;
        this.message = message;
    }

    @Override
    public void run() {
        for (int i = 0; i < count; i++) {
            try {
                System.out.println(message + i);
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
