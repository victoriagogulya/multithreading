package exceptions;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        ExecutorService service = Executors.newCachedThreadPool(new ThreadFactoryHandler());

        for(int i = 0; i < 3; i++) {
            service.execute(new TaskRunnableWithException());
        }
        service.shutdown();
    }
}
