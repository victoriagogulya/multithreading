package exceptions;


public class TaskRunnableWithException implements Runnable {

    @Override
    public void run() {
        System.out.println("TaskRunnableWithException has been started!");
        throw new RuntimeException();
    }
}
