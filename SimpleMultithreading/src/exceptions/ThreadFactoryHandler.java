package exceptions;


import java.util.concurrent.ThreadFactory;

public class ThreadFactoryHandler implements ThreadFactory {

    @Override
    public Thread newThread(Runnable r) {
        System.out.println("Factory: creation...");
        Thread thread = new Thread(r);
        thread.setUncaughtExceptionHandler(new MyUncaughtExceptionHandler());
        return thread;
    }
}
