package synchronization;


public interface INumbersGenerator {

    int next();
}
