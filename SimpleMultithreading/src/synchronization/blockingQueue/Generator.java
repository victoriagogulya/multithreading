package synchronization.blockingQueue;


import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class Generator implements Runnable {

    private BlockingQueue<Person> queue;
    private final int count = 5;
    private String[] names = new String[]{
            "Victoria",
            "Anna",
            "Victor",
            "Jon",
            "Maggi"
    };
    private Random random = new Random();

    public Generator(BlockingQueue<Person> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try{
            while(!Thread.interrupted()){
                int number = random.nextInt(count);
                int age = random.nextInt(50);
                Person person = new Person(names[number], age);
                queue.put(person);
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            System.out.println("Generator interrupted");
        }
        System.out.println("Generator exit!");
    }
}
