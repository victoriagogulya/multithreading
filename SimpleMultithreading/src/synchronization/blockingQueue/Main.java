package synchronization.blockingQueue;


import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    public static void main(String[] args) {
        BlockingQueue<Person> queue = new LinkedBlockingQueue<>(3);
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.execute(new Generator(queue));
        service.execute(new Printer(queue));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Main interrupted");
        }

        service.shutdownNow();
    }
}
