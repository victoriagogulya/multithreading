package synchronization.blockingQueue;


import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class MainWithDaemons {
    public static void main(String[] args) {

        BlockingQueue<Person> queue = new LinkedBlockingQueue<>(3);

        Thread threadGenerator = new Thread(new Generator(queue));
        Thread threadPrinter = new Thread(new Printer(queue));

        threadGenerator.setDaemon(true);
        threadGenerator.start();
        threadPrinter.start();


        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Main interrupted");
        }

        threadPrinter.interrupt();
    }
}
