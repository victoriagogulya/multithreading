package synchronization.blockingQueue;


import java.util.concurrent.BlockingQueue;

public class Printer implements Runnable {

    private BlockingQueue<Person> queue;

    public Printer(BlockingQueue<Person> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            while(!Thread.interrupted()) {
                Person person = queue.take();
                System.out.println(person.toString());
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            System.out.println("Printer interrupted");
        }
        System.out.println("Printer exit");
    }
}
