package synchronization.monitor;


import synchronization.INumbersGenerator;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        INumbersGenerator generator = new NumbersGenerator();
        ExecutorService service = Executors.newCachedThreadPool();
            for(int i = 0; i < 10; i++)
                service.execute(new TaskForNumbers(generator, i));
        service.shutdown();
    }
}
