package synchronization.monitor;


import synchronization.INumbersGenerator;

public class NumbersGenerator implements INumbersGenerator{
    private int number;

    public NumbersGenerator() {}

    @Override
    public synchronized int next() {
        return ++number;
    }
}
