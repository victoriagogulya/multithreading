package synchronization.monitor;


import synchronization.INumbersGenerator;

public class TaskForNumbers implements Runnable {

    private final INumbersGenerator generator;
    private final int id;

    public TaskForNumbers(final INumbersGenerator generator, int id) {
        this.generator = generator;
        this.id = id;
    }

    @Override
    public void run() {
        System.out.println("ID " + id + ": start generation...");
        int number = generator.next();
        System.out.println("ID " + id + ": result: " + number);
    }
}
