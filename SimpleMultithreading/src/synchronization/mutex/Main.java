package synchronization.mutex;


import synchronization.INumbersGenerator;
import synchronization.monitor.TaskForNumbers;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {

        ExecutorService service = Executors.newFixedThreadPool(3);
        INumbersGenerator generator = new NumbersGeneratorWithLock();

        for(int i = 0; i < 3; i++)
            service.execute(new TaskForNumbers(generator, i));
        service.shutdown();
    }
}
