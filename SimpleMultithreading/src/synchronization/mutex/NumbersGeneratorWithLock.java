package synchronization.mutex;


import synchronization.INumbersGenerator;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class NumbersGeneratorWithLock implements INumbersGenerator {

    private int number;
    private Lock lock = new ReentrantLock();

    @Override
    public int next() {
        lock.lock();
        try {
            return ++number;
        }finally {
            lock.unlock();
        }
    }
}
