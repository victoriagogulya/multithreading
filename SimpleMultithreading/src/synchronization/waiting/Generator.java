package synchronization.waiting;


public class Generator {

    private boolean odd;

    public synchronized void odd() {
        System.out.println("Odd!");
        odd = true;
        notifyAll();
    }

    public synchronized void even() {
        System.out.println("Even!");
        odd = false;
        notifyAll();
    }

    public synchronized void waitingOdd() throws InterruptedException {
        while(!odd) {
            wait();
        }
    }

    public synchronized void waitingEven() throws InterruptedException {
        while(odd) {
            wait();
        }
    }
}
