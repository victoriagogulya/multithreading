package synchronization.waiting;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        Generator generator = new Generator();
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.execute(new TaskOdd(generator));
        service.execute(new TaskEven(generator));

        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            System.out.println("Interrupted exc in Main");
        }
        service.shutdownNow();
        System.out.println("Exit!!!");
    }
}
