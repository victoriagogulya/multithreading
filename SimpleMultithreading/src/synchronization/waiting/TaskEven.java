package synchronization.waiting;


public class TaskEven implements Runnable {

    private Generator generator;

    public TaskEven(Generator generator) {
        this.generator = generator;
    }

    @Override
    public void run() {
        try{
            while(!Thread.interrupted()) {
                generator.waitingOdd();
                generator.even();
            }
        }catch (InterruptedException e) {
            System.out.println("Interrupted exc in TaskEven");
        }
        System.out.println("TaskEven exit");
    }
}
