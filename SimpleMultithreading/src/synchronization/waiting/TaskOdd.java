package synchronization.waiting;


public class TaskOdd implements Runnable {

    private Generator generator;

    public TaskOdd(Generator generator) {
        this.generator = generator;
    }

    @Override
    public void run() {
        try{
            while(!Thread.interrupted()) {
                generator.waitingEven();
                generator.odd();
            }
        }catch (InterruptedException e) {
            System.out.println("Interrupted exc in TaskOdd");
        }
        System.out.println("TaskOdd exit");
    }
}
